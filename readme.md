## Deskripsi
Ini adalah aplikasi untuk melakukan pull dari repo lain, sehingga aplikasi otomatis menjalankan update yang paling baru.

## Kebutuhan
- Install [NodeJS](https://nodejs.org/en/)
- Install [PM2](http://pm2.keymetrics.io/)

## Cara menggunakan
- Install library yang ada di `package.json` dengan perintah `npm install`
- Lakukan perubahan pada `path` dan `secret code` di `app.js`, sesuaikan dengan pengaturan `Gitlab Integration` di proyek yang akan dipasang webhook.
- Buat file configurasi dengan format `.yml`, contohnya `pm2-config.yml`. File ini berisikan konfigurasi aplikasi yang dipantau PM2 supaya tetap berjalan ketika terjadi kesalahan atau restart sistem.
- Tugaskan PM2 untuk menjalankan aplikasi webhook (proyek ini) dengan perintah `pm2 start path/to/pm2-config.yml`.
- Simpan state terakhir, supaya ketika sistem restart PM2 menjalankan kembali konfigurasi yang sudah didaftarkan, dengan perintah `pm2 save`.