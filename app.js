var http = require('http')
var createHandler = require('node-gitlab-webhook')
var shell = require('shelljs')

var handler = createHandler([
  { path: '/path', secret: 'secretcode' },
])

http.createServer(function (req, res) {
  handler(req, res, function (err) {
    res.statusCode = 404
    res.end('no such location')
  })
}).listen(3000)

handler.on('error', function (err) {
  console.error('Error:', err.message)
})

handler.on('push', function (event) {
  console.log(
    'Received a push event for %s to %s',
    event.payload.repository.name,
    event.payload.ref
  )
  switch(event.path) {
    case '/path':
        shell.cd('/var/www/project')
        shell.exec('git pull origin master')
        console.log('pull completed!')
      break
  }

})
